package com.example.nychighschool_santosh.ui.theme

enum class Destination {
    HOME,
    DETAIL
}