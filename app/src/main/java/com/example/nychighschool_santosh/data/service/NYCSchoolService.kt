package com.example.nychighschool_santosh.data.service

import com.example.nychighschool_santosh.data.NYCSchool
import retrofit2.Response
import retrofit2.http.GET

interface NYCSchoolService {

    @GET("/resource/s3k6-pzi2.json")
    suspend fun getNYCSchoolList(): Response<List<NYCSchool>>
}