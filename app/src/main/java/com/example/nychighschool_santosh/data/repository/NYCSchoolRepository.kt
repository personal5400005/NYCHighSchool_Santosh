package com.example.nychighschool_santosh.data.repository

import com.example.nychighschool_santosh.data.NYCSchool
import com.example.nychighschool_santosh.data.service.NYCSchoolService
import javax.inject.Inject

// TODO Define interface and use it everywhere as DI, instead of injecting implementation class in constructor.
//  Advantage: Makes it easier to junit test.
class NYCSchoolRepository @Inject constructor(
    private val nycSchoolService: NYCSchoolService
) {

    // TODO Handle error, failure and loading states.
    suspend fun getNYCSchoolList(): List<NYCSchool> {
        val response = nycSchoolService.getNYCSchoolList()
        return if (response.isSuccessful) {
            response.body() ?: emptyList()
        } else {
            emptyList()
        }
    }
}