package com.example.nychighschool_santosh.data

import com.example.nychighschool_santosh.data.repository.NYCSchoolRepository
import com.example.nychighschool_santosh.data.service.NYCSchoolService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class RepositoryModule {

    @Provides
    fun getNycSchoolRepository(service: NYCSchoolService) = NYCSchoolRepository(service)
}