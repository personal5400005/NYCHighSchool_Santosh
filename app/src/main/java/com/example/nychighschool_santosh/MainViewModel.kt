package com.example.nychighschool_santosh

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nychighschool_santosh.data.NYCSchool
import com.example.nychighschool_santosh.data.repository.NYCSchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: NYCSchoolRepository,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
): ViewModel() {

    private val _nycSchoolList = MutableStateFlow<List<NYCSchool>>(emptyList())
    val nycSchoolList: StateFlow<List<NYCSchool>> = _nycSchoolList

    private val _nycSchoolUserSelection = MutableStateFlow(NYCSchool())
    val nycSchoolUserSelection: StateFlow<NYCSchool> = _nycSchoolUserSelection

    // TODO Add Coroutine Name and Exception Handler to CoroutineContext.
    //  Benefits: Easier to debug due to coroutine name and Exception handler for logging purposes or to handle failure events.
    // TODO Handle network failed response.
    fun getNYCSchoolInfoList() {
        viewModelScope.launch(dispatcher) {
            val nycSchoolList = repository.getNYCSchoolList()
            _nycSchoolList.emit(nycSchoolList)
        }
    }

    // TODO Use jetpack compose navigation to pass arguments, instead of caching the value in VM.
    //  Unless there is future requirement to cache user selection to use it later.
    fun updateUserSelection(nycSchool: NYCSchool) {
        _nycSchoolUserSelection.value = nycSchool
    }
}