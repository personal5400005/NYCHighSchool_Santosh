package com.example.nychighschool_santosh

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolApp: Application() {
}