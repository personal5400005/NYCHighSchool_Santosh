package com.example.nychighschool_santosh

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.nychighschool_santosh.data.NYCSchool
import com.example.nychighschool_santosh.ui.theme.Destination
import com.example.nychighschool_santosh.ui.theme.NYCHighSchool_SantoshTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainViewModel.getNYCSchoolInfoList()

        setContent {
            NYCHighSchool_SantoshTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()

                    NavHost(navController = navController, startDestination = Destination.HOME.name) {
                        composable(Destination.HOME.name) {
                            val nycList by mainViewModel.nycSchoolList.collectAsState(initial = emptyList())
                            DisplayNYCList(nycList = nycList) { nycSchool ->
                                mainViewModel.updateUserSelection(nycSchool)
                                navController.navigate(Destination.DETAIL.name)
                            }
                        }

                        composable(Destination.DETAIL.name) {
                            val nycSchool by mainViewModel.nycSchoolUserSelection.collectAsState(
                                initial = NYCSchool()
                            )
                            
                            DisplayNYCDetail(nycSchool = nycSchool)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun DisplayNYCList(nycList: List<NYCSchool>, onClick: (nycSchool: NYCSchool) -> Unit) {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(nycList.size) {
            Row(modifier = Modifier
                .fillMaxWidth()
                .height(60.dp)
                .padding(20.dp)
                .clickable {
                    onClick.invoke(nycList[it])
                }) {
                Text(text = nycList[it].schoolName ?: "", fontSize = 14.sp)

                Text(text = nycList[it].dbn ?: "", modifier = Modifier.padding(horizontal = 10.dp), fontSize = 14.sp)
            }
        }
    }
}

@Composable
fun DisplayNYCDetail(nycSchool: NYCSchool) {
    Column(modifier = Modifier
        .fillMaxSize()
        .padding(20.dp)) {
        Text(text = nycSchool.overviewParagraph ?: "")
    }
}

@Preview
@Composable
fun DisplayNYCListPreview() {
    // TODO Populate Sample data.
    DisplayNYCList(nycList = emptyList()) {
        /* no-op */
    }
}