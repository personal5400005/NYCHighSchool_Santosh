package com.example.nychighschool_santosh

import com.example.nychighschool_santosh.data.NYCSchool
import com.example.nychighschool_santosh.data.repository.NYCSchoolRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.cancel
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock

class MainViewModelTest {

    private lateinit var mainViewModel: MainViewModel

    @RelaxedMockK
    private lateinit var repository: NYCSchoolRepository

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private val testScope = TestScope(mainCoroutineRule.testDispatcher)

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        mainViewModel = MainViewModel(repository, mainCoroutineRule.testDispatcher)
    }

    @Test
    fun testServiceResponseFailure() = testScope.runTest {
        coEvery { repository.getNYCSchoolList() } returns emptyList()
        mainViewModel.getNYCSchoolInfoList()

        assert(mainViewModel.nycSchoolList.value.isEmpty())

        coroutineContext.cancelChildren()
    }

    @Test
    fun testServiceResponseWithNonEmptySchoolListSuccess() = testScope.runTest {
        coEvery { repository.getNYCSchoolList() } returns listOf(NYCSchool(
            dbn = "02M260",
            schoolName = "Clinton School Writers & Artists, M.S. 260",
            overviewParagraph = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform."
        ))
        mainViewModel.getNYCSchoolInfoList()

        mainViewModel.nycSchoolList.apply {
            assert(value.size == 1)
            assert(value[0].dbn == "02M260")
            assert(value[0].schoolName == "Clinton School Writers & Artists, M.S. 260")
            assert(value[0].overviewParagraph == "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform.")
        }
    }

    @Test
    fun testUserSelectionNYCSchoolItem() = testScope.runTest {
        val testNYCSchool = NYCSchool(
            dbn = "02M260",
            schoolName = "Clinton School Writers & Artists, M.S. 260",
            overviewParagraph = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform."
        )
        mainViewModel.updateUserSelection(testNYCSchool)

        assert(mainViewModel.nycSchoolUserSelection.value.overviewParagraph == "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform.")

        coroutineContext.cancelChildren()
    }
}